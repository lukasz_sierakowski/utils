package com.here.hbi.utils.databrickstoavroschema;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;

public class Main {
    private static final HashMap<String, String> typeMapping = new HashMap<String, String>() {{
        put("smallint", "long");
        put("string", "string");
        put("int", "long");
        put("bigint", "long");
        put("boolean", "boolean");
        put("double", "double");
        put("timestamp", "long");
        put("map<string,string>", "string");
    }};

    public static void main(String[] args) throws IOException {
        final String template = "{\"name\":\"%s\",\"type\":[\"null\",\"%s\"],\"default\":null},";
        Reader in = new FileReader(args[0]);
        Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(in);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        for (CSVRecord record : records) {
            stringBuilder.append(System.getProperty("line.separator"));
            String columnName = record.get("col_name");
            String type = record.get("data_type");
            //System.out.println(columnName + " " + type);
            if (!typeMapping.containsKey(type)) {
                System.out.println("Missing type: " + type + " for culumn " + columnName);
                System.exit(1);
            }
            stringBuilder.append(String.format(template, columnName, typeMapping.get(type)));
        }
        // Remove last unnecessary comma
        stringBuilder.replace(stringBuilder.length() - 1, stringBuilder.length(), "");
        stringBuilder.append(System.getProperty("line.separator"));
        stringBuilder.append("]");
        System.out.println(stringBuilder.toString());
    }
}
