## Databricks to Avro schema converter

### Overview

This application allows converting Databricks database table schema stored in `CSV` file to List of fields used
by `Avro` schema.

### How to use

1. Prepare CSV file with database table schema
    1. Execute command in Databricks.
       
       ```DESCRIBE TABLE <table_name>;```
    3. Save result as `CSV` file.
    4. Run application with argument (path to saved `CSV` file).
    5. Wait for console output results.
